set_cpu_max_perf_pct
====================

Set global CPU usage at boot time (using systemd).

Copyright (c) 2016 Francesco Montanari, used under [CC0 license](https://creativecommons.org/publicdomain/zero/1.0/).

**Disclaimer**: The actions described below require root privileges and can be potential harmful.

Configuration
-------------

Set the max CPU use percentage in the `start()` field of the script `set_cpu_max_perf_pct`.
Copy this script into the `/usr/bin/` folder

Copy the unit file `set_cpu_max_perf_pct.service` into the `/etc/systemd/system/` folder.

Usage
-----

To start the service:
```
systemctl start set_cpu_max_perf_pct
```

Stop the service (i.e., revert to 100% usage)
```
systemctl stop set_cpu_max_perf_pct
```

Start the service at boot time:
```
systemctl enable set_cpu_max_perf_pct
```

Disable it if no longer required at boot time:
```
systemctl disable set_cpu_max_perf_pct
```

Alternative user script
-----------------------
The `user_set_cpu_max_perf_pct.sh` can be used if loading at boot is not needed.

Before running it for the first time, make the file owned by root and group root:
```
sudo chown root.root user_set_cpu_max_perf_pct.sh
```
Make it executable for all and writable only by root
```
sudo chmod 4755 user_set_cpu_max_perf_pct.sh
```

At any later time you can change the maximum cpu level in the script as needed, and run it:
```
./user_set_cpu_max_perf_pct.sh
```
